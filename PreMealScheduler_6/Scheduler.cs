﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.File;
using System.Diagnostics;
using System.Data.SqlClient;
namespace PreMealScheduler_6
{
    class Scheduler
    {
        public static StreamReader fileStreamReader;
        public static DataTable fileDataTable = new DataTable();
        public static string connstring = ConfigurationSettings.AppSettings["azureDBConnectionstring"].ToString();
        /// <summary>
        /// This is the entry point of the application
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            LogProcess("-------------------------Scheduler #6 Begins @ " + DateTime.Now.ToString() + "----------------------");
            StartProcess();
            LogProcess("-------------------------Scheduler #6 Ends @ " + DateTime.Now.ToString() + "-----------------------" +
                Environment.NewLine);
        }
        public static void StartProcess()
        {
            try
            {
                //Process ONLY when file exist in the Azure File Storage Location
                if (CheckIfNewFileExist())
                {
                    LogProcess("input file named premealsel.csv found");
                    //Reads the input file
                    ReadFile();
                    LogProcess("File Data is Read and loaded into Data Table");
                    //Populates the selection table
                    PopulateSelectionTable();                    
                    LogProcess("Populated tmp_selections table");
                    //deletes the input file
                    DeleteFile();
                    LogProcess("File is deleted from Azure storage location");
                    //Select peole to notify via SMS
                    SelectPeopleToNotify();
                    LogProcess("Populated tmp_offer table");
                    //Place a dummy file to simulate scheduler #7
                    PlaceADummyFile();
                    LogProcess("Dummy File Placed");

                }
                else
                {
                    LogProcess("No input file named premealsel.csv NOT found in the azure file storage");
                }
            }
            catch (Exception e)
            {
                LogProcess("Exception while executing function " + new StackTrace(e).GetFrame(0).GetMethod().Name +
                    "with the Error " + e.Message);
                //Log the error in pm_log table
                LogError(e.Message, new StackTrace(e).GetFrame(0).GetMethod().Name);
            }
        }
        /// <summary>
        /// This function deletes the file from the azure file storage location once processing is done
        /// </summary>
        public static void DeleteFile()
        {
            try
            {
                // Parse the connection string for the storage account.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationSettings.AppSettings["azurestorage"].ToString());

                // Create a CloudFileClient object for credentialed access to File storage.
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();

                // Get a reference to the file share we created previously.
                CloudFileShare share = fileClient.GetShareReference(ConfigurationSettings.AppSettings["azurefilesharename"].ToString());

                // Ensure that the share exists.
                if (share.Exists())
                {
                    CloudFileDirectory rootDirectory = share.GetRootDirectoryReference();
                    if (rootDirectory.Exists())
                    {
                        CloudFile file = rootDirectory.GetFileReference(ConfigurationSettings.AppSettings["azurefilename"].ToString());
                        if (file.Exists())
                        {
                            file.Delete();
                            file = null;
                            rootDirectory = null;
                            share = null;
                            fileClient = null;
                            storageAccount = null;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// This function is used to log error in table
        /// </summary>
        /// <param name="error">error message to be logged</param>
        /// <param name="method">method that threw the error</param>
        public static void LogError(string error, string method)
        {
            var connection = GetSqlConnection(connstring);
            SqlCommand cmd = new SqlCommand("InsertLog", connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@date", SqlDbType.VarChar).Value = DateTime.Now.ToString();
                cmd.Parameters.Add("@desc", SqlDbType.VarChar).Value = error;
                cmd.Parameters.Add("@method", SqlDbType.VarChar).Value = method;
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Dispose();
                cmd.Dispose();
            }
        }
        /// <summary>
        /// This function is used to check if new file exist in azure file storage location
        /// </summary>
        /// <returns></returns>
        public static bool CheckIfNewFileExist()
        {
            try
            {
                // Parse the connection string for the storage account.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationSettings.AppSettings["azurestorage"].ToString());

                // Create a CloudFileClient object for credentialed access to File storage.
                CloudFileClient fileClient = storageAccount.CreateCloudFileClient();

                // Get a reference to the file share we created previously.
                CloudFileShare share = fileClient.GetShareReference(ConfigurationSettings.AppSettings["azurefilesharename"].ToString());

                // Ensure that the share exists.
                if (share.Exists())
                {
                    CloudFileDirectory rootDirectory = share.GetRootDirectoryReference();
                    if (rootDirectory.Exists())
                    {
                        CloudFile file = rootDirectory.GetFileReference(ConfigurationSettings.AppSettings["azurefilename"].ToString());
                        if (file.Exists())
                        {
                            //upload stream reader from file data
                            fileStreamReader = new StreamReader(file.OpenRead());
                            file = null;
                            rootDirectory = null;
                            share = null;
                            fileClient = null;
                            storageAccount = null;
                            return true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return false;
        }
        /// <summary>
        /// This function is used to log step by process in log.txt found in azure file storage location.
        /// </summary>
        /// <param name="message">message to be logged</param>
        public static void LogProcess(string message)
        {
            // Parse the connection string for the storage account.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationSettings.AppSettings["azurestorage"].ToString());

            // Create a CloudFileClient object for credentialed access to File storage.
            CloudFileClient fileClient = storageAccount.CreateCloudFileClient();

            // Get a reference to the file share we created previously.
            CloudFileShare share = fileClient.GetShareReference(ConfigurationSettings.AppSettings["azurefilesharename"].ToString());

            // Ensure that the share exists.
            if (share.Exists())
            {
                CloudFileDirectory rootDirectory = share.GetRootDirectoryReference();
                if (rootDirectory.Exists())
                {
                    CloudFile file = rootDirectory.GetFileReference(ConfigurationSettings.AppSettings["LogFile"].ToString());
                    if (file.Exists())
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append(file.DownloadText());
                        sb.Append(Environment.NewLine);
                        sb.Append(message);
                        file.UploadText(sb.ToString());
                        file = null;
                        rootDirectory = null;
                        share = null;
                        fileClient = null;
                        storageAccount = null;
                    }
                }
            }
        }
        /// <summary>
        /// This function is used to read the file
        /// </summary>
        public static void ReadFile()
        {
            try
            {
                string[] columnNames = null;
                //  Get the first line
                string line = fileStreamReader.ReadLine();
                columnNames = line.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                //  Add the columns to the data table
                foreach (string colName in columnNames)
                    fileDataTable.Columns.Add(colName);
                //  Read the file
                string[] columns;
                while ((line = fileStreamReader.ReadLine()) != null)
                {
                    columns = line.Split(new string[] { "," }, StringSplitOptions.None);
                    if (!columns[3].Trim().Equals(string.Empty))
                    {
                        if (Convert.ToDateTime(columns[3]).ToString("d").Equals(DateTime.Now.ToString("d")))
                        {
                            fileDataTable.Rows.Add(columns);
                        }
                    }                    
                }
                columnNames = columns = null;
                line = null;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// This function is used to select people to notify via SMS
        /// </summary>
        public static void SelectPeopleToNotify()
        {
            var connection = GetSqlConnection(connstring);
            SqlCommand cmd = new SqlCommand("PopulateTmpOfferToSendSMS", connection);
            try
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Dispose();
                cmd.Dispose();
            }
        }
        /// <summary>
        /// This function is used to populated selection table and insert into pm_offer table
        /// </summary>
        public static void PopulateSelectionTable()
        {
            var connection = GetSqlConnection(connstring);
            SqlCommand cmd = new SqlCommand("PopulatePmOfferFromMail", connection);
            try
            {
                SqlBulkCopy bulkcopy = new SqlBulkCopy(connstring);
                bulkcopy.DestinationTableName = ConfigurationSettings.AppSettings["MainTable"].ToString();
                bulkcopy.WriteToServer(fileDataTable);
                LogProcess("Data from Data table is loaded into pm_offer Table");
                bulkcopy.Close();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
                LogProcess("Data from TMP_Selections is loaded into pm_offer Table");
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                connection.Dispose();
                cmd.Dispose();
            }
        }
        /// <summary>
        /// This function is used to get SQL Connection for future use
        /// </summary>
        /// <param name="conn">DB Connection string</param>
        /// <returns></returns>
        private static SqlConnection GetSqlConnection(string conn)
        {
            try
            {
                var connection = new SqlConnection(conn);
                connection.Open();
                return connection;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// This function is used to place a dummy file so that scheduler #7 can be kick started
        /// </summary>
        public static void PlaceADummyFile()
        {
            // Parse the connection string for the storage account.
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationSettings.AppSettings["azurestorage"].ToString());

            // Create a CloudFileClient object for credentialed access to File storage.
            CloudFileClient fileClient = storageAccount.CreateCloudFileClient();

            // Get a reference to the file share we created previously.
            CloudFileShare share = fileClient.GetShareReference(ConfigurationSettings.AppSettings["azurefilesharename"].ToString());

            // Ensure that the share exists.
            if (share.Exists())
            {
                CloudFileDirectory rootDirectory = share.GetRootDirectoryReference();
                if (rootDirectory.Exists())
                {
                    CloudFile file = rootDirectory.GetFileReference(ConfigurationSettings.AppSettings["dummyfile"].ToString());
                    file.Create(0);
                    file = null;
                    rootDirectory = null;
                    share = null;
                    fileClient = null;
                    storageAccount = null;
                }
            }
        }
    }    
}
